+++
date = 2019-11-30T20:37:00Z
image = ""
showonlyimage = false
title = "Fotoeffekt"
weight = "2"

+++
Der Fotoeffekt

<!--more-->

# Fotoeffekt

[Fotoeffekt.pdf](/uploads/Fotoeffekt.pdf "Fotoeffekt.pdf")

{{< pdf width="90%" height="800" path="/uploads/Fotoeffekt.pdf" fallback="[Fotoeffekt.pdf](/uploads/Fotoeffekt.pdf 'Fotoeffekt.pdf')" >}}