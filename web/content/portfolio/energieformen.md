+++
date = 2019-11-30T20:12:47Z
image = ""
showonlyimage = false
title = "Energieformen"
weight = "1"

+++
Die Energieformen der Physik

<!--more-->

# Energieformen

[EnergieFormen.pdf](/uploads/EnergieFormen.pdf "EnergieFormen.pdf")
{{< pdf width="90%" height="800" path="/uploads/EnergieFormen.pdf" fallback="[EnergieFormen.pdf](/uploads/EnergieFormen.pdf 'EnergieFormen.pdf')" >}}