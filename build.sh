#!/bin/sh
for d in Documents/*/ ; do
    cd "$d"
    latexmk -xelatex Document.tex
    cd ../..
done